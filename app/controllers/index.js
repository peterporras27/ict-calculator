
/* This is a single context application with multiple windows in a stack */
var calc = require('calc');
var osname = Ti.Platform.osname,
	version = Ti.Platform.version,
	height = Ti.Platform.displayCaps.platformHeight,
	width = Ti.Platform.displayCaps.platformWidth;
var area = '0';

var r1 = '21%', r2 = '37%', r3 = '53%', r4 = '69%', r5 = '85%', r6='1%';
var c1 = '1%', c2 = '26%', c3 ='51%', c4 = '76%';
var pos = {
	a:{t:r1,l:c1}, b:{t:r1,l:c2}, c:{t:r1,l:c3}, d:{t:r1,l:c4},
	e:{t:r2,l:c1}, f:{t:r2,l:c2}, g:{t:r2,l:c3}, h:{t:r2,l:c4},
	i:{t:r3,l:c1}, j:{t:r3,l:c2}, k:{t:r3,l:c3}, l:{t:r3,l:c4},
	m:{t:r4,l:c1}, n:{t:r4,l:c2}, o:{t:r4,l:c3}, p:{t:r4,l:c4},
	q:{t:r5,l:c1}, r:{t:r5,l:c2}, s:{t:r5,l:c3}, t:{t:r5,l:c4},
	u:{t:r5,l:c1}, v:{t:r6,l:c2}, w:{t:r6,l:c3}, x:{t:r6,l:c4},
};

var isIOS = (Ti.Platform.osname === 'iphone' || Ti.Platform.osname === 'ipad'),
	self = Ti.UI.createView({
	    width: '100%',
	    height: '100%',
	    top: '0',
	    left: '0',
	    backgroundColor:'#ccc'
	}),
	display = Ti.UI.createLabel({
        color:'#EEEEEE',
        backgroundColor: '#444444',
        text: area,
        top: '0',
        left: '0',
        height:'10%',
        width:'100%',
        textAlign: 'right',
        font: {fontSize: 32}
    }),
    display2 = Ti.UI.createLabel({
        color:'#00ff00',
        backgroundColor: '#444444',
        text:'',
        top: '9%',
        left: '0',
        height:'10%',
        width:'100%',
        textAlign: 'right',
        font: {fontSize: 32}
    }),
    keyPad = Ti.UI.createView ({
	    width: '100%',
	    height: '83%',
	    top: '16%',
	    left: 0
	}),
	
	textColor = '#333',
	oppositeTextColor = '#FFF',
	backColor = '#fff',
	sysBackColor = '#333',
	radius = 10,
	btnHeight = '15%',
	btnWidth = '23%',
	
	buttonDetails = Ti.UI.createButton({
        color: oppositeTextColor,
        backgroundColor: '#6d6d6d',
        borderRadius: radius,
        title:'Info',
        top: '5%',
        left: '1%',
        height: btnHeight,
        width:btnWidth,
        font: {fontWeight: 'bold'}
    }),
    
    buttonANS = Ti.UI.createButton({
        color: oppositeTextColor,
        backgroundColor: '#6d6d6d',
        borderRadius: radius,
        title:'ANS',
        top: '5%',
        left: pos.b.l,
        height:btnHeight,
        width:btnWidth,
        font: {fontWeight: 'bold'}
    }),
    
    buttonCE = Ti.UI.createButton({
        color: oppositeTextColor,
        backgroundColor: '#31708f',
        borderRadius: radius,
        title:'CE',
        top: '5%',
        left: '51%',
        height: btnHeight,
        width:'48%',
        font: {fontWeight: 'bold'}
	}),
    
    buttonB = Ti.UI.createButton({
        color: oppositeTextColor,
        backgroundColor: '#6d6d6d',
        borderRadius: radius,
        title:'B',
        top: pos.a.t,
        left: pos.a.l,
        height: btnHeight,
        width:btnWidth,
        font: {fontWeight: 'bold'}
    }),
    buttonO = Ti.UI.createButton({
        color: oppositeTextColor,
        backgroundColor: '#6d6d6d',
        borderRadius: radius,
        title:'O',
        top: pos.b.t,
        left: pos.b.l,
        height:btnHeight,
        width:btnWidth,
        font: {fontWeight: 'bold'}
    }),
    buttonH = Ti.UI.createButton({
        color: oppositeTextColor,
        backgroundColor: '#6d6d6d',
        borderRadius: radius,
        title:'H',
        top: pos.c.t,
        left: pos.c.l,
        height:btnHeight,
        width:btnWidth,
        font: {fontWeight: 'bold'}
    }),
    buttonC = Ti.UI.createButton({
        color: oppositeTextColor,
        backgroundColor: '#31708f',
        borderRadius: radius,
        title:'C',
        top: pos.d.t,
        left: pos.d.l,
        height:btnHeight,
        width:btnWidth,
        font: {fontWeight: 'bold'}
    }),
    button7 = Ti.UI.createButton({
        color: textColor,
        backgroundColor: backColor,
        borderRadius: radius,
        title:'7',
        top: pos.e.t,
        left: pos.e.l,
        height:btnHeight,
        width:btnWidth,
        font: {fontWeight: 'bold'}
    }),
    button8 = Ti.UI.createButton({
        color: textColor,
        backgroundColor: backColor,
        borderRadius: radius,
        title:'8',
        top: pos.f.t,
        left: pos.f.l,
        height:btnHeight,
        width:btnWidth,
        font: {fontWeight: 'bold'}
    }),
    button9 = Ti.UI.createButton({
        color: textColor,
        backgroundColor: backColor,
        borderRadius: radius,
        title:'9',
        top: pos.g.t,
        left: pos.g.l,
        height:btnHeight,
        width:btnWidth,
        font: {fontWeight: 'bold'}
    }),
    buttonDivide = Ti.UI.createButton({
        color: oppositeTextColor,
        backgroundColor: sysBackColor,
        borderRadius: radius,
        title:'\u00F7',
        top: pos.h.t,
        left: pos.h.l,
        height:btnHeight,
        width:btnWidth,
        font: {fontSize: 28,fontWeight: 'bold'}
    }),
    button4 = Ti.UI.createButton({
        color: textColor,
        backgroundColor: backColor,
        borderRadius: radius,
        title:'4',
        top: pos.i.t,
        left: pos.i.l,
        height:btnHeight,
        width:btnWidth,
        font: {fontWeight: 'bold'}
    }),
    button5 = Ti.UI.createButton({
        color: textColor,
        backgroundColor: backColor,
        borderRadius: radius,
        title:'5',
       	top: pos.j.t,
        left: pos.j.l,
        height:btnHeight,
        width:btnWidth,
        font: {fontWeight: 'bold'}
    }),
    button6 = Ti.UI.createButton({
        color: textColor,
        backgroundColor: backColor,
        borderRadius: radius,
        title:'6',
       	top: pos.k.t,
        left: pos.k.l,
        height:btnHeight,
        width:btnWidth,
        font: {fontWeight: 'bold'}
    }),
    buttonMultiply = Ti.UI.createButton({
        color: oppositeTextColor,
        backgroundColor: sysBackColor,
        borderRadius: radius,
        title:'\u00D7',
        top: pos.l.t,
        left: pos.l.l,
        height:btnHeight,
        width:btnWidth,
        font: {fontSize: 28,fontWeight: 'bold'}
    }),
    button1 = Ti.UI.createButton({
        color: textColor,
        backgroundColor: backColor,
        borderRadius: radius,
        title:'1',
        top: pos.m.t,
        left: pos.m.l,
        height:btnHeight,
        width:btnWidth,
        font: {fontWeight: 'bold'}
    }),
    button2 = Ti.UI.createButton({
        color: textColor,
        backgroundColor: backColor,
        borderRadius: radius,
        title:'2',
        top: pos.n.t,
        left: pos.n.l,
        height:btnHeight,
        width:btnWidth,
        font: {fontWeight: 'bold'}
    }),
    button3 = Ti.UI.createButton({
        color: textColor,
        backgroundColor: backColor,
        borderRadius: radius,
        title:'3',
        top: pos.o.t,
        left: pos.o.l,
        height:btnHeight,
        width:btnWidth,
        font: {fontWeight: 'bold'}
    }),
    buttonSubtract = Ti.UI.createButton({
        color: oppositeTextColor,
        backgroundColor: sysBackColor,
        borderRadius: radius,
        title:'\u2212',
        top: pos.p.t,
        left: pos.p.l,
        height:btnHeight,
        width:btnWidth,
        font: {fontSize: 28,fontWeight: 'bold'}
    }),
    buttonDecimal = Ti.UI.createButton({
        color: textColor,
        backgroundColor: backColor,
        borderRadius: radius,
        title:'.',
        top: pos.q.t,
        left: pos.q.l,
        height:btnHeight,
        width:btnWidth,
        font: {fontSize: 28,fontWeight: 'bold'}
    }),
    button0 = Ti.UI.createButton({
        color: textColor,
        backgroundColor: backColor,
        borderRadius: radius,
        title:'0',
        top: pos.r.t,
        left: pos.r.l,
        height:btnHeight,
        width:btnWidth,
        font: {fontWeight: 'bold'}
    }),
    buttonEqual = Ti.UI.createButton({
        color: oppositeTextColor,
        backgroundColor: '#f0ad4e',
        borderRadius: radius,
        title:'=',
        top: pos.s.t,
        left: pos.s.l,
        height:btnHeight,
        width:btnWidth,
        font: {fontSize: 28,fontWeight: 'bold'}
    }),
    buttonAdd = Ti.UI.createButton({
        color: oppositeTextColor,
        backgroundColor: sysBackColor,
        borderRadius: radius,
        title:'\u002B',
        top: pos.t.t,
        left: pos.t.l,
        height:btnHeight,
        width:btnWidth,
        font: {fontSize: 28,fontWeight: 'bold'}
    });

keyPad.add( buttonDetails );
keyPad.add( buttonANS );
keyPad.add( buttonCE );

keyPad.add( buttonB );
keyPad.add( buttonO );
keyPad.add( buttonH );
keyPad.add( buttonC );

keyPad.add( button0 );
keyPad.add( button1 );
keyPad.add( button2 );
keyPad.add( button3 );
keyPad.add( button4 );
keyPad.add( button5 );
keyPad.add( button6 );
keyPad.add( button7 );
keyPad.add( button8 );
keyPad.add( button9 );
keyPad.add( buttonAdd );
keyPad.add( buttonDecimal );
keyPad.add( buttonDivide );
keyPad.add( buttonEqual );
keyPad.add( buttonMultiply );
keyPad.add( buttonSubtract );

self.add( display );
self.add( display2 );
self.add( keyPad );

$.index.add( self );
$.index.open();

$.index.addEventListener('android:back',function(e){
    $.index.close();
});
/* Watch for a keypad event */
keyPad.addEventListener('click', function(e) {
    /* filter out all none keypad events */
	if( e.source.title ) {
		
		var area = display.getText();
 		var key = e.source.title;
 		
 		var plus = area.toString().indexOf('\u002B');
		var minus = area.toString().indexOf('\u2212');
		var divide = area.toString().indexOf('\u00F7');
		var multiply = area.toString().indexOf('\u00D7');
		var hasDot = area.toString().indexOf('.');
		var op = null;
	
		if ( plus > 0 ) { op = '\u002B'; }
		else if ( minus > 0 ) { op = '\u2212'; }
		else if ( divide > 0 ) { op = '\u00F7'; }
		else if ( multiply > 0 ) { op = '\u00D7'; }
		
		switch( key ){
			case 'ANS': 
			
				var keys = ['\u002B','\u2212','\u00F7','\u00D7'];
				var ans = Alloy.Globals.ans;
				
				if ( keys.indexOf(op) >= 0 ){
					
					var x = area.toString().split(op)[0];
					var y = area.toString().split(op)[1];
					
					var newDisplay = ( x && y ) ? ans:area+''+ans;
					display.setText( newDisplay );
					display2.setText( '' );
					
				} else {
					
					ans = ( ans == '' ) ? '0':ans;
					display.setText( ans );
					display2.setText( '' );
				}
				
			break;
			case 'Info': 
				
				if ( area!='0' ) {
					Alloy.createController('details',{
						home: $.index,
						display: display,
						display2: display2
					}).getView().open();
				}
				
			break;
			case 'CE': 
				calc.clearError( display );
			break;
			case 'B': 
				calc.convertMe( key, display, display2 );
			break;
			case 'O': 
				calc.convertMe( key, display, display2 );
			break;
			case 'H': 
				calc.convertMe( key,display, display2 );
			break;
			case 'C': 
				calc.clearDisplay( display, display2 );
			break;
			case '.': 
				calc.appendPeriod( key, display, op, plus, minus, divide, multiply );
			break;
			case '=': 
				calc.solution( display, display2, plus, minus, divide, multiply );
			break;
			default: 
				calc.calculate( key, display, display2, plus, minus, divide, multiply );
			break;
		}
	}
});