// Arguments passed into this controller can be accessed via the `$.args` object directly or:
var args = $.args;
var calc = require('calc');

calc.convertMe( 'B', args.display, args.display2 );
calc.convertMe( 'O', args.display, args.display2 );
calc.convertMe( 'H', args.display, args.display2 );

$.details.addEventListener('android:back',function(e){
    $.details.close();
});

Ti.App.addEventListener('app:fromWebView', function() {
	var c = require('calc');
	Ti.App.fireEvent('app:fromTitanium', { 
		binary: c.binary,
		octa: c.octa,
		hexa: c.hexa, 
	});
});