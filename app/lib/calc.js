var displayText = '';

exports.binary = '';
exports.hexa = '';
exports.octa = '';

function add(x, y) {
    return x + y;
}

function divide(x, y) {
    if (y === 0) {
        alert("Can't divide by 0");
        return 0;
    }
    return x / y;
}

function multiply(x, y) {
    return x * y;
}

function subtract(x, y) {
    return x - y;
}

/* clears all of the digits */
exports.clearDisplay = function( display, display2 ) {
    display.setText(0);
    display2.setText('');
};
/* removes the last digit entered in the display */
exports.clearError = function( display ){
	
	var text = display.getText();
    /* if the string is valid, remove the right most character from it remember: to be valid, must have a value and length */
    if( text ){
        text = text.slice(0, text.length - 1);
        text = text? text: "0";
    }
    
    display.setText( text );
};


function updateResult( value ){
    Ti.App.fireEvent('calc.updateResult', {display: value});
}

function updateDisplay( value ){
    Ti.App.fireEvent('calc.updateDisplay', {display: value});
}

exports.convertMe = function( type, display, display2 ){
	
	var val = display.getText();
	var values = [], binary = [], y = 2, show = '';

	switch( type ){
		case 'B': y = 2; break;
		case 'O': y = 8; break;
		case 'H': y = 16; break;
	}
	
	var hexa = {10:'A',11:'B',12:'C',13:'D',14:'E',15:'F'};
	// loop through possible values
	for (var i = 30 - 1; i >= 0; i--) {
		// get the index if the number has a decimal point
		var hasDot = val.toString().indexOf('.');
		// check and fetch the nondecimal numbers
		var toDevide = ( hasDot > 0 ) ? val.toString().split('.')[0]: val;
		// divide the number by 2
		var v1 = parseFloat( toDevide ) / y; 
		// overide variable to prepare next value
		val = v1;
		// check if there's still need for dividing
		if ( val > 0 ) {
			// save answer among the list
			values.push( v1 );
			// get the index if the number has a decimal point
			var hasDot = v1.toString().indexOf('.');

			if ( type=='B' ) {
				// convert to binary
				var bin = ( hasDot > 0 ) ? 1:0;

			} else if( type=='H' ) {

				var rem = parseFloat( toDevide ) % parseFloat(y); 
				if ( rem >= 10 && rem <= 15 ) {
					var bin = hexa[rem];
				} else {
					var bin = rem;
				}
				
			} else {

				var bin = parseFloat( toDevide ) % y;
			}
			
			// save binary code
			binary.push( bin );
			show += '<tr><td>'+toDevide+' /  '+y+' </td><td>'+ v1 +'</td><td><strong>'+bin+'</strong></td></tr>';
		}
	}
	
	// reverse binary code order and join
	var result = binary.reverse().join('').toString();
	
	display2.setText( result );
	show += '<tr><td><strong>Answer:</strong></td><td></td><td><strong>'+result+'</strong></td></tr>';
	switch(type){
		case 'B':
			this.binary = show;
		break;
		case 'O':
			this.octa = show;
		break;
		case 'H':
			this.hexa = show;
		break;
	}
};

exports.appendPeriod = function( key, area, op, plus, minus, divide, multiply ){
	
	var text = area.getText();
	if ( text == 0 ) {
		text = '';
	}
	
	var hasDot = text.toString().indexOf('.');
	
	// check if operation is found
	if ( plus > 0 || minus > 0 || divide > 0 || multiply > 0 ) {
		
		if ( op ) {

			var y = text.toString().split(op)[1];							
			var yDot = y.toString().indexOf('.');

			// check if there is dot
			if ( hasDot < 0 ) { 
				
				text += key; 

			} else {

				if( yDot < 0 ) { text += key; }
			}
			
		} else {

			if ( hasDot < 0 ) { text += key; }
		}

	} else {

		if ( hasDot < 0 ) { text += key; }
	}
	
	area.setText(text);
};

exports.solution = function( area, output, plus, minus, divide, multiply ){
	
	var text = area.getText();
	// plus found
	if ( plus > 0 ) {
		
		var x = text.toString().split('\u002B')[0];
		var y = text.toString().split('\u002B')[1];
		var res = ( x && y ) ? parseFloat(x)+parseFloat(y):'';
		var period = res.toString().indexOf('.');
		if ( period > 0 ) { res = res.toFixed(4); }
		output.setText( res );
		Alloy.Globals.ans = res;

	} else if ( minus > 0 ) {
		
		var x = text.toString().split('\u2212')[0];
		var y = text.toString().split('\u2212')[1];
		var res = ( x && y ) ? parseFloat(x)-parseFloat(y):'';
		var period = res.toString().indexOf('.');
		if ( period > 0 ) { res = res.toFixed(4); }
		output.setText( res );
		Alloy.Globals.ans = res;

	} else if ( divide > 0 ) {
		
		var x = text.toString().split('\u00F7')[0];
		var y = text.toString().split('\u00F7')[1];
		var res = ( x && y ) ? parseFloat(x)/parseFloat(y):'';
		var period = res.toString().indexOf('.');
		if ( period > 0 ) { res = res.toFixed(4); }
		output.setText( res );
		Alloy.Globals.ans = res;


	} else if ( multiply > 0 ) {

		var x = text.toString().split('\u00D7')[0];
		var y = text.toString().split('\u00D7')[1];
		var res = ( x && y ) ? parseFloat(x)*parseFloat(y):'';
		var period = res.toString().indexOf('.');
		if ( period > 0 ) { res = res.toFixed(4); }
		output.setText( res );
		Alloy.Globals.ans = res;

	}
};

exports.calculate = function( key, area, display2, plus, minus, divide, multiply ){
	
	var text = area.getText();
	var ans = display2.getText();
	
	if ( text == 0 ) {
		text = '';
	}
	
	var keys = ['\u002B','\u2212','\u00F7','\u00D7'];
	if ( text == 0 && keys.indexOf(key) >= 0 ) {
		return;
	}
	
	if ( key == '\u002B' ) {
		
		// operation not found
		if ( plus < 0 ) { 

			if ( minus > 0 || divide > 0 || multiply > 0 ) {

			} else {
				
				text += key; 
			}
			
		}	
		
		if ( ans && area ) { 
			text = ans+''+key;
			display2.setText(''); 
		}

	} else if ( key == '\u2212' ){

		// operation not found
		if ( minus < 0 ) { 
			
			if ( plus > 0 || divide > 0 || multiply > 0 ) {
				
			} else {
				text += key; 
			}
			
		}
		
		if ( ans && area ) { 
			text = ans+''+key;
			display2.setText(''); 
		}

	} else if ( key == '\u00F7' ){

		// operation not found
		if ( divide < 0 ) { 
			
			if ( minus > 0 || plus > 0 || multiply > 0 ) {
				
			} else {
				text += key; 
			}
			
		}
		
		if ( ans && area ) { 
			text = ans+''+key;
			display2.setText(''); 
		}

	} else if ( key == '\u00D7' ){

		// operation not found
		if ( multiply < 0 ) { 
			
			if ( minus > 0 || divide > 0 || plus > 0 ) {
				
			} else {

				text += key; 
			}
			
		}
		
		if ( ans && area ) { 
			text = ans+''+key;
			display2.setText(''); 
		}

	} else {

		text += key;
	}
	
	area.setText(text);
};